# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Application middlewares."""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.httpsredirect import HTTPSRedirectMiddleware
from fastapi.middleware.trustedhost import TrustedHostMiddleware

from .asgi import app
from ..conf import settings
from ..db.middleware import DBSessionMiddleware


def configure_app_middlewares(application: FastAPI) -> None:
    """Configure application middlewares.

    Add your middlewares here, this makes this function testable.
    """
    application.add_middleware(
        TrustedHostMiddleware,
        allowed_hosts=settings.ALLOWED_HOSTS,
    )

    application.add_middleware(
        CORSMiddleware,
        allow_origins=settings.ALLOWED_ORIGINS,
        allow_methods=['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH'],
        allow_headers=['Authorization', 'Content-Type'],
        allow_credentials=False,
        max_age=24 * 3600,  # It's capped on most browsers anyway
    )

    if settings.ENABLE_HTTPS_REDIRECT:
        application.add_middleware(HTTPSRedirectMiddleware)

    application.add_middleware(DBSessionMiddleware)


configure_app_middlewares(app)
