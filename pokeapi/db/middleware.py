# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Database session middleware."""

import logging

from fastapi import Request
from fastapi import Response
from fastapi import status
from fastapi.responses import JSONResponse
from sqlalchemy.exc import IntegrityError
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.middleware.base import RequestResponseEndpoint

from . import SessionLocal

logger = logging.getLogger(__name__)


class DBSessionMiddleware(BaseHTTPMiddleware):
    """Database session middleware."""

    async def dispatch(
            self,
            request: Request,
            call_next: RequestResponseEndpoint,
    ) -> Response:
        """Handle a request adding a database session to it."""
        # noinspection PyBroadException
        try:
            request.state.db = SessionLocal()
            response = await call_next(request)
        except IntegrityError:
            # ToDo: limit requests, otherwise this enables an oracle!
            logger.exception('Database integrity error')
            response = JSONResponse(
                content={'detail': 'Requested duplication of a unique value'},
                status_code=status.HTTP_400_BAD_REQUEST,
            )
        except:  # noqa
            # The service shall not fall, but exception must be logged
            logger.exception('Fatal exception occurred')
            response = JSONResponse(
                content={'detail': 'Internal server error'},
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        finally:
            request.state.db.close()

        return response
