# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Endpoint responses documentation."""

from pydantic import BaseModel


class BadRequestResponse(BaseModel):
    """Bad request response schema."""

    detail: str


general_responses = {
}

bad_request_response = {
    '400': {
        'model': BadRequestResponse,
    },
}
