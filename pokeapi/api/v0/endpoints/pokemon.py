# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Pokemon endpoints."""

import typing

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Path
from fastapi import status

from pokeapi import models
from pokeapi.db import SessionLocal
from ._responses import general_responses
from .. import schemas
from .. import selectors
from .. import services
from ...shortcuts import get_db

router = APIRouter()

PokemonIdPath = Path(..., gt=0, lt=1e15)


@router.get(
    '/',
    response_model=typing.List[schemas.pokemon.PokemonReadResponse],
    responses={
        **general_responses,
    },
)
async def list_pokemon(
        *,
        db: SessionLocal = Depends(get_db),
) -> typing.List[models.Pokemon]:
    """List all Pokemon."""
    pokemon: typing.List[models.Pokemon] = await selectors.pokemon.get_pokemon(
        db=db,
    )
    return pokemon


@router.post(
    '/',
    response_model=schemas.pokemon.PokemonCreateResponse,
    status_code=status.HTTP_201_CREATED,
    responses={
        **general_responses,
        '404': {
            'description': 'Given Trainer not found',
            'model': schemas.pokemon.PokemonNotFoundResponse,
        },
    },
)
async def create_pokemon(
        *,
        data: schemas.pokemon.PokemonCreateRequest,
        db: SessionLocal = Depends(get_db),
) -> models.Pokemon:
    """Create a Pokemon and optionally assign it to a trainer."""
    pokemon: models.Pokemon = await services.pokemon.create_pokemon(
        db=db,
        **data.dict(),
    )

    return pokemon


@router.get(
    '/{pokemon_id}/',
    response_model=schemas.pokemon.PokemonReadResponse,
    responses={
        **general_responses,
        '404': {
            'description': 'Pokemon not found',
            'model': schemas.pokemon.PokemonNotFoundResponse,
        },
    },
)
async def read_pokemon(
        *,
        pokemon_id: int = PokemonIdPath,
        db: SessionLocal = Depends(get_db),
) -> models.Pokemon:
    """Read Pokemon details."""
    pokemon: models.Pokemon = await selectors.pokemon.get_pokemon(
        db=db,
        id=pokemon_id,
    )

    return pokemon


@router.put(
    '/{pokemon_id}/',
    response_model=schemas.pokemon.PokemonUpdateResponse,
    responses={
        **general_responses,
        '404': {
            'description': 'Pokemon or Trainer not found',
            'model': schemas.pokemon.PokemonNotFoundResponse,
        },
    },
)
async def change_pokemon_trainer(
        *,
        pokemon_id: int = PokemonIdPath,
        data: schemas.pokemon.PokemonUpdateRequest,
        db: SessionLocal = Depends(get_db),
) -> models.Pokemon:
    """Change trainer for a pokemon."""
    pokemon: models.Pokemon = await services.pokemon.change_trainer(
        db=db,
        id=pokemon_id,
        **data.dict(),
    )

    return pokemon


@router.delete(
    '/{pokemon_id}/',
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        **general_responses,
        '404': {
            'description': 'Pokemon not found',
            'model': schemas.pokemon.PokemonNotFoundResponse,
        },
    },
)
async def delete_pokemon(
        *,
        pokemon_id: int = PokemonIdPath,
        db: SessionLocal = Depends(get_db),
) -> None:
    """Delete a Pokemon."""
    await services.pokemon.delete_pokemon(
        db=db,
        id=pokemon_id,
    )
