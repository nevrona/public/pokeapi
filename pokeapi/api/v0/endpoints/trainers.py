# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Trainer endpoints."""

import typing

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Path
from fastapi import status

from pokeapi import models
from pokeapi.db import SessionLocal
from ._responses import bad_request_response
from ._responses import general_responses
from .. import schemas
from .. import selectors
from .. import services
from ...shortcuts import get_db

router = APIRouter()

TrainerIdPath = Path(..., gt=0, lt=1e15)


@router.get(
    '/',
    response_model=typing.List[schemas.trainer.TrainerReadResponse],
    responses={
        **general_responses,
    },
)
async def list_trainers(
        *,
        db: SessionLocal = Depends(get_db),
) -> typing.List[models.Trainer]:
    """List all trainers."""
    trainers: typing.List[models.Trainer] = await selectors.trainer.get_trainers(
        db=db,
    )
    return trainers


@router.post(
    '/',
    response_model=schemas.trainer.TrainerCreateResponse,
    status_code=status.HTTP_201_CREATED,
    responses={
        **general_responses,
        **bad_request_response,
    },
)
async def create_trainer(
        *,
        data: schemas.trainer.TrainerCreateRequest,
        db: SessionLocal = Depends(get_db),
) -> models.Trainer:
    """Create a trainer."""
    trainer: models.Trainer = await services.trainer.create_trainer(
        db=db,
        **data.dict(),
    )

    return trainer


@router.get(
    '/{trainer_id}/',
    response_model=schemas.trainer.TrainerReadResponse,
    responses={
        **general_responses,
        '404': {
            'description': 'Trainer not found',
            'model': schemas.trainer.TrainerNotFoundResponse,
        },
    },
)
async def read_trainer(
        *,
        trainer_id: int = TrainerIdPath,
        db: SessionLocal = Depends(get_db),
) -> models.Trainer:
    """Read a trainer details."""
    trainer: models.Trainer = await selectors.trainer.get_trainer(
        db=db,
        id=trainer_id,
    )

    return trainer


@router.put(
    '/{trainer_id}/',
    response_model=schemas.trainer.TrainerUpdateResponse,
    responses={
        **general_responses,
        **bad_request_response,
        '404': {
            'description': 'Trainer not found',
            'model': schemas.trainer.TrainerNotFoundResponse,
        },
    },
)
async def update_trainer(
        *,
        trainer_id: int = TrainerIdPath,
        data: schemas.trainer.TrainerUpdateRequest,
        db: SessionLocal = Depends(get_db),
) -> models.Trainer:
    """Update a trainer details."""
    trainer: models.Trainer = await services.trainer.update_trainer(
        db=db,
        id=trainer_id,
        **data.dict(),
    )

    return trainer


@router.delete(
    '/{trainer_id}/',
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        **general_responses,
        '404': {
            'description': 'Trainer not found',
            'model': schemas.trainer.TrainerNotFoundResponse,
        },
    },
)
async def delete_trainer(
        *,
        trainer_id: int = TrainerIdPath,
        db: SessionLocal = Depends(get_db),
) -> None:
    """Delete a trainer."""
    await services.trainer.delete_trainer(
        db=db,
        id=trainer_id,
    )
