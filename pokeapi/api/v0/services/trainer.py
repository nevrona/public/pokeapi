# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Trainer services."""

import typing

from pokeapi import crud
from pokeapi import exceptions
from pokeapi import models
from pokeapi.db import SessionLocal


async def delete_trainer(
        *,
        db: SessionLocal,
        id: int,  # noqa: A002
) -> None:
    """Delete a trainer.

    :raise ObjectNotFound: Trainer not found.
    """
    trainer_crud: crud.Trainer = crud.Trainer(db)
    trainer: typing.Optional[models.Trainer] = await trainer_crud.read(
        one_or_none=True,
        filters=[
            models.Trainer.id == id,
        ],
    )
    if not trainer:
        raise exceptions.ObjectNotFound('trainer not found')

    await trainer_crud.delete()


async def update_trainer(
        *,
        db: SessionLocal,
        id: int,  # noqa: A002
        name: str,
) -> models.Trainer:
    """Update a trainer.

    :raise ObjectNotFound: Trainer not found.
    """
    trainer_crud: crud.Trainer = crud.Trainer(db)
    trainer: typing.Optional[models.Trainer] = await trainer_crud.read(
        one_or_none=True,
        filters=[
            models.Trainer.id == id,
        ],
    )
    if not trainer:
        raise exceptions.ObjectNotFound('trainer not found')

    return await trainer_crud.update(name=name)


async def create_trainer(
        *,
        db: SessionLocal,
        name: str,
) -> models.Trainer:
    """Create a trainer."""
    trainer: models.Trainer = await crud.Trainer(db).create(name=name)
    return trainer
