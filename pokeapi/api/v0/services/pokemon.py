# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Pokemon services."""

import typing

from pokeapi import crud
from pokeapi import exceptions
from pokeapi import models
from pokeapi.db import SessionLocal
from .. import selectors


async def create_pokemon(
        *,
        db: SessionLocal,
        number: int,
        trainer: typing.Optional[int],
) -> models.Pokemon:
    """Create a pokemon."""
    if trainer:
        # Check if it exists
        await selectors.trainer.get_trainer(
            db=db,
            id=trainer,
        )

    pokemon = await crud.Pokemon(db).create(number=number, trainer_id=trainer)
    return pokemon


async def change_trainer(
        *,
        db: SessionLocal,
        id: int,  # noqa: A002
        trainer: int,
) -> models.Pokemon:
    """Change trainer for this pokemon.

    :raise ObjectNotFound: Pokemon or trainer not found.
    """
    pokemon_crud: crud.Pokemon = crud.Pokemon(db)
    pokemon: typing.Optional[models.Pokemon] = await pokemon_crud.read(
        one_or_none=True,
        filters=[
            models.Pokemon.id == id,
        ],
    )
    if not pokemon:
        raise exceptions.ObjectNotFound('pokemon not found')

    # Check if it exists
    await selectors.trainer.get_trainer(
        db=db,
        id=trainer,
    )

    return await pokemon_crud.update(trainer_id=trainer)


async def delete_pokemon(
        *,
        db: SessionLocal,
        id: int,  # noqa: A002
) -> None:
    """Delete given pokemon.

    :raise ObjectNotFound: Pokemon not found.
    """
    pokemon_crud: crud.Pokemon = crud.Pokemon(db)
    pokemon: typing.Optional[models.Pokemon] = await pokemon_crud.read(
        one_or_none=True,
        filters=[
            models.Pokemon.id == id,
        ],
    )
    if not pokemon:
        raise exceptions.ObjectNotFound('pokemon not found')

    await pokemon_crud.delete()
