# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Pokemon schemas."""

import typing

from pydantic import BaseModel
from pydantic import Field


class PokemonNotFoundResponse(BaseModel):
    """Pokemon not found response schema."""

    detail: str


class TrainerDetail(BaseModel):
    """Trainer detail schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    name: str = Field(..., max_length=50, min_length=5)

    class Config:
        """Schema config."""

        orm_mode = True


class PokemonCreateRequest(BaseModel):
    """Pokemon creation request schema."""

    number: int = Field(..., gt=0, lt=150)
    trainer: typing.Optional[int] = Field(None, gt=0, lt=1e15)


class PokemonCreateResponse(BaseModel):
    """Pokemon creation response schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    number: int = Field(..., gt=0, lt=150)
    trainer: typing.Optional[TrainerDetail]

    class Config:
        """Schema config."""

        orm_mode = True


class PokemonReadResponse(BaseModel):
    """Pokemon read response schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    number: int = Field(..., gt=0, lt=150)
    trainer: typing.Optional[TrainerDetail]

    class Config:
        """Schema config."""

        orm_mode = True


class PokemonUpdateRequest(BaseModel):
    """Pokemon update request schema."""

    trainer: int = Field(..., gt=0, lt=1e15)


class PokemonUpdateResponse(BaseModel):
    """Pokemon update response schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    number: int = Field(..., gt=0, lt=150)
    trainer: typing.Optional[TrainerDetail]

    class Config:
        """Schema config."""

        orm_mode = True
