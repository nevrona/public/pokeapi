# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Trainer schemas."""

import typing

from pydantic import BaseModel
from pydantic import Field


class TrainerNotFoundResponse(BaseModel):
    """Pokemon not found response schema."""

    detail: str


class PokemonDetail(BaseModel):
    """Pokemon detail schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    number: int = Field(..., gt=0, lt=150)

    class Config:
        """Schema config."""

        orm_mode = True


class TrainerReadResponse(BaseModel):
    """Trainer read response schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    name: str = Field(..., max_length=50, min_length=5)
    pokemon: typing.List[PokemonDetail]

    class Config:
        """Schema config."""

        orm_mode = True


class TrainerCreateRequest(BaseModel):
    """Trainer create request schema."""

    name: str = Field(..., max_length=50, min_length=5)


class TrainerCreateResponse(BaseModel):
    """Trainer create response schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    name: str = Field(..., max_length=50, min_length=5)
    pokemon: typing.List[PokemonDetail]

    class Config:
        """Schema config."""

        orm_mode = True


class TrainerUpdateRequest(BaseModel):
    """Trainer update request schema."""

    name: str = Field(..., max_length=50, min_length=5)


class TrainerUpdateResponse(BaseModel):
    """Trainer update response schema."""

    id: int = Field(..., gt=0, lt=1e15)  # noqa: A003  # sqlite int max
    name: str = Field(..., max_length=50, min_length=5)
    pokemon: typing.List[PokemonDetail]

    class Config:
        """Schema config."""

        orm_mode = True
