# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Pokemon selectors."""

import typing

from pokeapi import crud
from pokeapi import exceptions
from pokeapi import models
from pokeapi.db import SessionLocal


async def get_pokemon(
        *,
        db: SessionLocal,
        id: typing.Optional[int] = None,  # noqa: A002
        offset: int = 0,
        limit: int = 20,
) -> typing.Union[models.Pokemon, typing.List[models.Pokemon]]:
    """Get a pokemon or a List of pokemon.

    :raise ObjectNotFound: The pokemon was not found.
    """
    if id:
        one_or_none = True
        filters = [
            models.Pokemon.id == id,
        ]
    else:
        one_or_none = False
        filters = None

    pokemon: typing.Union[
        typing.List[models.Pokemon],
        models.Pokemon,
        None,
    ]
    pokemon = await crud.Pokemon(db).read(
        one_or_none=one_or_none,
        filters=filters,
        offset=offset,
        limit=limit,
    )
    if pokemon is None:
        raise exceptions.ObjectNotFound('pokemon not found')

    return pokemon
