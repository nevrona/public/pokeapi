"""Pokemon CRUD."""

from .base import Crud
from .. import models


class Pokemon(Crud):
    """Pokemon crud."""

    model = models.Pokemon
