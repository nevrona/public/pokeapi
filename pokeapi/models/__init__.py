# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Expose models."""

from sqlalchemy_utils import force_auto_coercion

# Assign automatic data type coercion for all classes which are of certain type
# This needs to be done before importing models.
force_auto_coercion()

# Import all models here to leave them ready for migrations and the app
from .pokemon import Pokemon  # noqa: E402
from .trainer import Trainer  # noqa: E402

# Expose models below
__all__ = (
    'Pokemon',
    'Trainer',
)
