# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Pokemon model."""

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from .mixins import PrimaryKeyMixin
from ..db import Base


class Pokemon(Base, PrimaryKeyMixin):
    """Pokemon model."""

    number = sa.Column(sa.Integer, nullable=False)
    trainer_id = sa.Column(sa.Integer, sa.ForeignKey('trainer.id'), nullable=True)

    trainer = relationship('Trainer', back_populates='pokemon')
