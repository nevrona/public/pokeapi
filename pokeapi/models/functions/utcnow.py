# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""UTCNOW function for SQLAlchemy to get datetime as TZ aware in UTC.

See:
https://docs.sqlalchemy.org/en/13/core/compiler.html?highlight=utcnow#utc-timestamp-function
"""

from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql import expression
from sqlalchemy.types import DateTime


class utcnow(expression.FunctionElement):  # noqa: N801
    """Get the current datetime aware in UTC."""

    type = DateTime(timezone=True)  # noqa: A003


@compiles(utcnow, 'postgresql')
def pg_utcnow(element, compiler, **kw):
    """Holder for Postgres to get the current timestamp in UTC."""
    # The first TIMEZONE returns a naive datetime, whereas the second makes it
    # aware.
    return "TIMEZONE('utc', TIMEZONE('utc', statement_timestamp())"


@compiles(utcnow, 'cockroachdb')
def cr_utcnow(element, compiler, **kw):
    """Holder for Cockroach to get the current timestamp in UTC."""
    # The first TIMEZONE returns a naive datetime, whereas the second makes it
    # aware.
    return "TIMEZONE(TIMEZONE(statement_timestamp(), 'utc'), 'utc')"


@compiles(utcnow, 'sqlite')
def sl_utcnow(element, compiler, **kw):
    """Holder for SQLite to get the current timestamp in UTC."""
    # SQLite doesn't deal with aware datetime
    return "DATETIME('now')"
