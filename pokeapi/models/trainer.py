# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Trainer models."""

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from .mixins import PrimaryKeyMixin
from ..db import Base


class Trainer(Base, PrimaryKeyMixin):
    """Trainer model."""

    name = sa.Column(sa.String(length=50), nullable=False, unique=True, index=True)

    pokemon = relationship('Pokemon', back_populates='trainer')
