"""Model mixins."""

from secrets import randbits

import sqlalchemy as sa


def generate_id() -> int:
    """Generate a random integer id."""
    return randbits(48)  # just in case...


class PrimaryKeyMixin:
    """Primary key `id` field as random integer lower than 53 bits.

    JSON spec integers maximum value is 53.
    """

    id = sa.Column(  # noqa: A003
        sa.Integer,
        primary_key=True,
        autoincrement=False,
        default=generate_id,
        unique=True,
        index=True,
        nullable=False,
    )
