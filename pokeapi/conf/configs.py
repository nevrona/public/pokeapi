# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Prepare all settings and leave them ready to be used by the app."""

import os
from functools import lru_cache

from .checks import configure_settings
from .global_settings import ENV_PREFIX
from .global_settings import Settings
from .helpers import froze_settings
from .helpers import set_python_logging_config
from .helpers import unfroze_settings_class
from .settings_for_tests import TestSettings

# Allow settings override
try:
    from .local_settings import LocalSettings
except ImportError:
    LocalSettings = None


def get_project_settings(*, use_local_settings: bool = False, **kwargs) -> Settings:
    """Get project settings.

    :param use_local_settings: Load local settings if found.
    :param kwargs: Extra settings values (overwrites any other environment value).
    """
    unfroze_settings_class(Settings)
    if use_local_settings and LocalSettings is not None:
        # noinspection PyCallingNonCallable
        kw_arguments = {**LocalSettings().dict(), **kwargs}  # kwargs has precedence
        settings = Settings(**kw_arguments)
    else:
        settings = Settings(**kwargs)
    configure_settings(settings)
    froze_settings(settings)
    set_python_logging_config(settings)
    return settings


def get_test_settings() -> Settings:
    """Get a setting instance to run tests."""
    settings = get_project_settings(**TestSettings().dict())
    return settings


@lru_cache()
def get_settings() -> Settings:
    """Get appropriate settings checking if settings for tests are required.

    This can be used as a FastAPI dependency.

    This function is cached.
    """
    testing: bool = os.getenv(f'{ENV_PREFIX}TESTING', '').lower() in ('true', '1')
    if testing:
        return get_test_settings()

    return get_project_settings(use_local_settings=True)
