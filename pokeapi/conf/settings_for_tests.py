# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Settings file to run tests.

Set here any setting that needs to overwrite a global setting.

Avoid using a file like this in production!
"""

import typing

from pydantic import BaseSettings


class TestSettings(BaseSettings):
    """Settings for testing the app.

    Set here any required setting with a certain value.
    """

    class Config:
        """Configurations for Pydantic settings class."""

        env_prefix = 'TESTING_'
        env_file = 'testing.env'
        use_enum_values = True

    APP_TITLE: str = 'fastapi'
    DEBUG: bool = True
    DEVELOPMENT_MODE: bool = False
    ENABLE_HTTPS_REDIRECT: bool = False
    ALLOWED_HOSTS: typing.List[str] = ['127.0.0.1']
    API_PREFIX: str = ''
    DATABASE_NAME: str = 'db.sqlite3'
    DATABASE_USER: str = ''
    DATABASE_PASSWORD: str = 'password'
