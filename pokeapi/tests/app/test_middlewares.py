from unittest import TestCase
from unittest import mock

from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.httpsredirect import HTTPSRedirectMiddleware
from fastapi.middleware.trustedhost import TrustedHostMiddleware

from ...app import middlewares
from ...db.middleware import DBSessionMiddleware


class TestAppMiddlewares(TestCase):

    @mock.patch.object(middlewares, 'settings')
    def test_configure_app_middlewares_with_https_redirect(self, mock_settings):
        mock_settings.ENABLE_HTTPS_REDIRECT = True
        app = mock.MagicMock()

        self.assertIsNone(middlewares.configure_app_middlewares(app))
        app.add_middleware.assert_has_calls([
            mock.call(
                TrustedHostMiddleware,
                allowed_hosts=mock_settings.ALLOWED_HOSTS,
            ),
            mock.call(
                CORSMiddleware,
                allow_origins=mock_settings.ALLOWED_ORIGINS,
                allow_methods=['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH'],
                allow_headers=['Authorization', 'Content-Type'],
                allow_credentials=False,
                max_age=24 * 3600,
            ),
            mock.call(
                HTTPSRedirectMiddleware,
            ),
            mock.call(
                DBSessionMiddleware,
            ),
        ])

    @mock.patch.object(middlewares, 'settings')
    def test_configure_app_middlewares_without_https_redirect(self, mock_settings):
        mock_settings.ENABLE_HTTPS_REDIRECT = False
        app = mock.MagicMock()

        self.assertIsNone(middlewares.configure_app_middlewares(app))
        app.add_middleware.assert_has_calls([
            mock.call(
                TrustedHostMiddleware,
                allowed_hosts=mock_settings.ALLOWED_HOSTS,
            ),
            mock.call(
                CORSMiddleware,
                allow_origins=mock_settings.ALLOWED_ORIGINS,
                allow_methods=['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH'],
                allow_headers=['Authorization', 'Content-Type'],
                allow_credentials=False,
                max_age=24 * 3600,
            ),
            mock.call(
                DBSessionMiddleware,
            ),
        ])
