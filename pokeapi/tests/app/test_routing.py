import os
from unittest import TestCase
from unittest import mock

from ...app import routing


class TestAPIRouting(TestCase):

    @mock.patch.object(routing, 'import_module')
    @mock.patch.object(routing, 'Path')
    @mock.patch.object(routing, 'settings')
    def test_find_and_add_apis(
            self,
            mock_settings,
            mock_path,
            mock_import_module,
    ):
        mock_settings.BASE_DIR = 'base'
        mock_settings.PACKAGE_NAME = 'package'
        apiv1_path = mock.MagicMock()
        apiv1_path.is_dir.return_value = True
        apiv1_path.name = 'v1'
        apiv2_path = mock.MagicMock()
        apiv2_path.is_dir.return_value = True
        apiv2_path.name = 'v2'
        mock_path.return_value.glob.return_value = (p for p in (apiv1_path, apiv2_path))
        rtr = mock.MagicMock()
        mock_import_module.return_value.api_router = rtr
        mock_import_module.return_value.version_prefix = 'version_prefix'
        application = mock.MagicMock()

        self.assertIsNone(routing.find_and_add_apis(application))
        mock_path.assert_called_with(os.path.join('base', 'api'))
        mock_path().glob.assert_called_once_with('v*/')
        mock_import_module.assert_has_calls([
            mock.call('.api.v1.urls', 'package'),
            mock.call('.api.v2.urls', 'package'),
        ])
        application.include_router.assert_has_calls([
            mock.call(rtr, prefix='version_prefix'),
            mock.call(rtr, prefix='version_prefix'),
        ])

    @mock.patch.object(routing, 'find_and_add_apis')
    @mock.patch.object(routing, 'settings')
    def test_configure_app_routers(
            self,
            mock_settings,
            mock_find_and_add_apis,
    ):
        mock_settings.DEVELOPMENT_MODE = False
        app = mock.MagicMock()
        root_router = mock.MagicMock()

        routing.configure_app_routers(app, root_router)
        app.include_router.assert_called_once_with(root_router, prefix='')
        mock_find_and_add_apis.assert_called_once_with(app)

    @mock.patch.object(routing, '_get_static_files')
    @mock.patch.object(routing, 'find_and_add_apis')
    @mock.patch.object(routing, 'settings')
    def test_configure_app_routers_dev_mode(
            self,
            mock_settings,
            mock_find_and_add_apis,
            mock_get_static_files,
    ):
        mock_settings.DEVELOPMENT_MODE = True
        mock_settings.STATIC_URL_PATH = '/static'
        mock_settings.BASE_DIR = ''
        app = mock.MagicMock()
        root_router = mock.MagicMock()

        routing.configure_app_routers(app, root_router)
        app.include_router.assert_called_once_with(root_router, prefix='')
        mock_find_and_add_apis.assert_called_once_with(app)
        mock_get_static_files.assert_called_once_with(directory='static')
        app.mount.assert_called_once_with(
            '/static',
            mock_get_static_files.return_value,
            name='static',
        )

    @mock.patch.object(routing, 'StaticFiles')
    def test_get_static_files(self, mock_static_files):
        routing._get_static_files(1, b=2)
        mock_static_files.assert_called_once_with(1, b=2)

    @mock.patch.object(routing, 'StaticFiles')
    def test_get_static_files_no_dependency_exception(self, mock_static_files):
        mock_static_files.side_effect = TypeError

        self.assertRaises(
            ImportError,
            routing._get_static_files,
        )
        mock_static_files.assert_called_once_with()
