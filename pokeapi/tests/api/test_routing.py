import os
from unittest import TestCase
from unittest import mock

from ...api import routing


class TestAPIRouting(TestCase):

    @mock.patch.object(routing, 'settings')
    def test_build_prefix(self, mock_settings):
        mock_settings.API_PREFIX = '/prefix'

        self.assertEqual(routing.build_prefix('/api'), '/prefix/api')

    @mock.patch.object(routing, 'settings')
    def test_get_static_asset_str(self, mock_settings):
        mock_settings.PACKAGE_NAME = 'package'
        mock_settings.STATIC_URL = None
        mock_settings.STATIC_URL_PATH = '/static/path'
        mock_settings.API_PREFIX = '/prefix'

        self.assertEqual(
            routing.get_static('asset', 'category'),
            '/prefix/static/path/package/category/asset',
        )

    @mock.patch.object(routing, 'settings')
    def test_get_static_asset_list(self, mock_settings):
        mock_settings.PACKAGE_NAME = 'package'
        mock_settings.STATIC_URL = None
        mock_settings.STATIC_URL_PATH = '/static/path'
        mock_settings.API_PREFIX = '/prefix'

        self.assertEqual(
            routing.get_static(['asset', 'list'], 'category'),
            '/prefix/static/path/package/category/asset/list',
        )

    @mock.patch.object(routing, 'settings')
    def test_get_static_url(self, mock_settings):
        mock_settings.PACKAGE_NAME = 'package'
        mock_settings.STATIC_URL = 'https://static.url'

        self.assertEqual(
            routing.get_static('asset', 'category'),
            'https://static.url/package/category/asset',
        )

    @mock.patch.object(routing, 'import_module')
    @mock.patch.object(routing, 'Path')
    @mock.patch.object(routing, 'settings')
    def test_find_and_add_endpoints(
            self,
            mock_settings,
            mock_path,
            mock_import_module,
    ):
        mock_settings.BASE_DIR = 'base'
        mock_settings.PACKAGE_NAME = 'package'
        endp1_path = mock.MagicMock()
        endp1_path.is_file.return_value = True
        endp1_path.name = 'endp1'
        endp1_path.suffix = ''
        endp2_path = mock.MagicMock()
        endp2_path.is_file.return_value = True
        endp2_path.name = 'endp2'
        endp2_path.suffix = ''
        mock_path.return_value.glob.return_value = (p for p in (endp1_path, endp2_path))
        rtr = mock.MagicMock()
        mock_import_module.return_value.router = rtr
        router = mock.MagicMock()
        version = 'a.b'

        self.assertIsNone(routing.find_and_add_endpoints(router, version))
        mock_path.assert_called_once_with(
            os.path.join('base', 'api', 'va_b', 'endpoints'),
        )
        mock_path().glob.assert_called_with('*.py')
        mock_import_module.assert_has_calls([
            mock.call('.api.va_b.endpoints.endp1', 'package'),
            mock.call('.api.va_b.endpoints.endp2', 'package'),
        ])
        router.include_router.assert_has_calls([
            mock.call(rtr, prefix='/endp1', tags=['endp1']),
            mock.call(rtr, prefix='/endp2', tags=['endp2']),
        ])
