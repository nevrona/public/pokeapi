from unittest import TestCase

from fastapi import FastAPI
from fastapi.testclient import TestClient

from ...api import root
from ...conf import settings


def create_app() -> FastAPI:
    app = FastAPI(docs_url=None, redoc_url=None)
    app.include_router(root.router)

    return app


class TestAPIRoot(TestCase):

    def setUp(self) -> None:
        app = create_app()
        client = TestClient(app)

        self.app = app
        self.client = client

    def test_read_the_docs(self):
        response = self.client.get('/', allow_redirects=False)
        self.assertEqual(response.status_code, 307)
        self.assertEqual(response.text, 'null')
        self.assertEqual(response.headers['location'], '/openapi.json')

    def test_read_the_docs_human(self):
        response = self.client.get(
            '/',
            allow_redirects=False,
            headers={'user-agent': 'Firefox'},
        )
        self.assertEqual(response.status_code, 307)
        self.assertEqual(response.text, 'null')
        self.assertEqual(response.headers['location'], '/docs')

    def test_swagger_ui_redirect_html(self):
        response = self.client.get('/docs/oauth2-redirect', allow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.text)

    def test_swagger_ui_html(self):
        response = self.client.get('/docs', allow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            f'/static/{settings.PACKAGE_NAME}/vendor/swagger/swagger-ui-bundle.js',
            response.text,
        )

    def test_redoc_html(self):
        response = self.client.get('/redoc', allow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            f'/static/{settings.PACKAGE_NAME}/vendor/redoc/redoc.standalone.js',
            response.text,
        )
