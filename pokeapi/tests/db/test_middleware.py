from unittest import TestCase
from unittest import mock

from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy.exc import IntegrityError

from ...db import middleware


def create_app():
    app = FastAPI()
    app.add_middleware(middleware.DBSessionMiddleware)

    @app.get('/')
    async def root():
        return {'hello': 'world'}

    @app.get('/integrity-error')
    async def raise_integrity_error():
        raise IntegrityError('some-error', None, None)

    @app.get('/value-error')
    async def raise_value_error():
        raise ValueError('some-value-error')

    return app


class TestDBMiddleware(TestCase):

    def setUp(self) -> None:
        app = create_app()
        client = TestClient(app)

        self.app = app
        self.client = client

    def test_view_no_exception(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'hello': 'world'})

    @mock.patch.object(middleware, 'logger')
    def test_view_value_error(self, mock_logger):
        response = self.client.get('/value-error')
        mock_logger.exception.assert_called_once()
        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), {'detail': 'Internal server error'})

    @mock.patch.object(middleware, 'logger')
    def test_view_integrity_error(self, mock_logger):
        response = self.client.get('/integrity-error')
        mock_logger.exception.assert_called_once()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {'detail': 'Requested duplication of a unique value'},
        )
