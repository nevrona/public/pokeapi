import asyncio
from datetime import datetime
from datetime import timezone
from unittest import TestCase
from unittest import mock

from ...db import utils


class DBUtilsTests(TestCase):

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_is_aware_requested_aware(self, mock_fn_utcnow):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        mock_fn_utcnow.return_value = now
        db = mock.MagicMock()
        db.query.return_value.one.return_value = [now]

        async def async_test():
            dt = await utils.utcnow(db, aware=True)
            self.assertEqual(dt, now)
            mock_fn_utcnow.assert_called_once()
            db.query.assert_called_once_with(now)
            db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_is_aware_requested_naive(self, mock_fn_utcnow):
        now_naive = datetime.utcnow()
        now = now_naive.replace(tzinfo=timezone.utc)
        mock_fn_utcnow.return_value = now
        db = mock.MagicMock()
        db.query.return_value.one.return_value = [now]

        async def async_test():
            dt = await utils.utcnow(db, naive=True)
            self.assertEqual(dt, now_naive)
            mock_fn_utcnow.assert_called_once()
            db.query.assert_called_once_with(now)
            db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_is_naive_requested_naive(self, mock_fn_utcnow):
        now_naive = datetime.utcnow()
        mock_fn_utcnow.return_value = now_naive
        db = mock.MagicMock()
        db.query.return_value.one.return_value = [now_naive]

        async def async_test():
            dt = await utils.utcnow(db, naive=True)
            self.assertEqual(dt, now_naive)
            mock_fn_utcnow.assert_called_once()
            db.query.assert_called_once_with(now_naive)
            db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_is_naive_requested_aware(self, mock_fn_utcnow):
        now_naive = datetime.utcnow()
        now = now_naive.replace(tzinfo=timezone.utc)
        mock_fn_utcnow.return_value = now_naive
        db = mock.MagicMock()
        db.query.return_value.one.return_value = [now_naive]

        async def async_test():
            dt = await utils.utcnow(db, aware=True)
            self.assertEqual(dt, now)
            mock_fn_utcnow.assert_called_once()
            db.query.assert_called_once_with(now_naive)
            db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_is_naive_requested_default(self, mock_fn_utcnow):
        now_naive = datetime.utcnow()
        mock_fn_utcnow.return_value = now_naive
        db = mock.MagicMock()
        db.query.return_value.one.return_value = [now_naive]

        async def async_test():
            dt = await utils.utcnow(db)
            self.assertEqual(dt, now_naive)
            mock_fn_utcnow.assert_called_once()
            db.query.assert_called_once_with(now_naive)
            db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_is_aware_requested_default(self, mock_fn_utcnow):
        now_naive = datetime.utcnow()
        now = now_naive.replace(tzinfo=timezone.utc)
        mock_fn_utcnow.return_value = now
        db = mock.MagicMock()
        db.query.return_value.one.return_value = [now]

        async def async_test():
            dt = await utils.utcnow(db)
            self.assertEqual(dt, now)
            mock_fn_utcnow.assert_called_once()
            db.query.assert_called_once_with(now)
            db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(utils, 'fn_utcnow')
    def test_utcnow_requested_aware_and_naive_raises_error(self, mock_fn_utcnow):
        db = mock.MagicMock()

        async def async_test():
            with self.assertRaises(ValueError):
                await utils.utcnow(db, naive=True, aware=True)

            db.query.assert_not_called()
            db.query.return_value.one.assert_not_called()
            mock_fn_utcnow.assert_not_called()

        asyncio.run(async_test())
