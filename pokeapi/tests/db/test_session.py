from unittest import TestCase
from unittest import mock

from ...db import session


class TestDBSession(TestCase):

    @mock.patch.object(session, 'create_engine')
    def test_get_engine(self, mock_create_engine):
        engine = mock.MagicMock()
        mock_create_engine.return_value = engine
        self.assertEqual(session.get_engine(), engine)
        mock_create_engine.assert_called_once()

    @mock.patch.object(session, 'sessionmaker')
    def test_get_session(self, mock_sessionmaker):
        engine = mock.MagicMock()
        sess = mock.MagicMock()
        mock_sessionmaker.return_value = sess
        self.assertEqual(session.get_session(engine), sess)
        mock_sessionmaker.assert_called_once_with(autoflush=False, bind=engine)
