from unittest import TestCase
from unittest import mock

from ...conf import gunicorn


class TestConfGunicorn(TestCase):

    def test_on_starting(self):
        server = mock.MagicMock()

        gunicorn.on_starting(server)

    def test_on_reload(self):
        server = mock.MagicMock()

        gunicorn.on_reload(server)

    def test_when_ready(self):
        server = mock.MagicMock()

        gunicorn.when_ready(server)

    def test_pre_fork(self):
        server = mock.MagicMock()
        worker = mock.MagicMock()

        gunicorn.pre_fork(server, worker)

    def test_post_fork(self):
        server = mock.MagicMock()
        worker = mock.MagicMock()

        gunicorn.post_fork(server, worker)

    def test_post_worker_init(self):
        worker = mock.MagicMock()

        gunicorn.post_worker_init(worker)

    def test_worker_int(self):
        worker = mock.MagicMock()

        gunicorn.worker_int(worker)

    def test_worker_abort(self):
        worker = mock.MagicMock()

        gunicorn.worker_abort(worker)

    def test_pre_exec(self):
        server = mock.MagicMock()

        gunicorn.pre_exec(server)

    def test_pre_request(self):
        worker = mock.MagicMock()
        req = mock.MagicMock()

        gunicorn.pre_request(worker, req)
        worker.log.debug.assert_called_once()

    def test_post_request(self):
        worker = mock.MagicMock()
        req = mock.MagicMock()
        environ = mock.MagicMock()
        resp = mock.MagicMock()

        gunicorn.post_request(worker, req, environ, resp)

    def test_child_exit(self):
        server = mock.MagicMock()
        worker = mock.MagicMock()

        gunicorn.child_exit(server, worker)

    def test_worker_exit(self):
        server = mock.MagicMock()
        worker = mock.MagicMock()

        gunicorn.worker_exit(server, worker)

    def test_nworkers_changed(self):
        server = mock.MagicMock()
        new_value = mock.MagicMock()
        old_value = mock.MagicMock()

        gunicorn.nworkers_changed(server, new_value, old_value)

    def test_on_exit(self):
        server = mock.MagicMock()

        gunicorn.on_exit(server)
