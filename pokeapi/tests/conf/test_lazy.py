from unittest import TestCase
from unittest import mock

from ...conf import lazy


class TestConfLazy(TestCase):

    @mock.patch.object(lazy, 'get_settings')
    @mock.patch.object(lazy.SimpleLazyObject, '__init__')
    def test_lazy_settings(self, mock_simple_lazy_object, mock_get_settings):
        obj = lazy.LazySettings()
        self.assertIsInstance(obj, lazy.LazySettings)
        mock_simple_lazy_object.assert_called_once_with(mock_get_settings)
