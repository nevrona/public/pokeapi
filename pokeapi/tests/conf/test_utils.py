from unittest import TestCase
from unittest import mock

from ...conf import utils


class TestConfUtils(TestCase):

    def test_get_project_version(self):
        mock_open = mock.mock_open(read_data='line1\nversion = "1.2.3"\nline3')

        with mock.patch.object(utils, 'open', new=mock_open, create=True):
            version = utils.get_project_version.__wrapped__()  # Bypass lru_cache
        self.assertEqual(version, '1.2.3')
        mock_open.assert_called_once()

    @mock.patch.object(utils, 'settings')
    def test_get_app_base_url(self, mock_settings):
        mock_settings.ALLOWED_HOSTS = ['test.nevrona.org']
        mock_settings.API_PREFIX = '/prefix/'  # Must strip final backslash

        url = utils.get_app_base_url()
        self.assertEqual(url, 'https://test.nevrona.org/prefix')

    def test_get_project_internal_version(self):
        mock_open = mock.mock_open(read_data='1.2.3')

        with mock.patch.object(utils, 'open', new=mock_open, create=True):
            version = utils.get_project_internal_version.__wrapped__()  # lru_cache
        self.assertEqual(version, '1.2.3')
        mock_open.assert_called_once()

    def test_get_project_internal_version_no_file(self):
        mock_open = mock.MagicMock()
        mock_open.side_effect = FileNotFoundError

        with mock.patch.object(utils, 'open', new=mock_open, create=True):
            version = utils.get_project_internal_version.__wrapped__()  # lru_cache
        self.assertEqual(version, '')  # Should return default value
        mock_open.assert_called_once()
