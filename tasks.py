# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Common tasks for Invoke."""

from invoke import task

from pokeapi.conf.defs import package_name
from pokeapi.conf.global_settings import ENV_PREFIX

# Set your registry URL
REGISTRY = 'registry.gitlab.com/nevrona/public/pokeapi'


@task(
    default=True,
    help={
        'development': 'run a development server'
    }
)
def runserver(ctx, development=False):
    """Run a development or production server."""
    if development:
        ctx.run(
            f'uvicorn {package_name}.app:app --reload',
            echo=True,
            pty=True,
            env={
                f'{ENV_PREFIX}ALLOWED_HOSTS': '["127.0.0.1"]',
                f'{ENV_PREFIX}DEVELOPMENT_MODE': 'true',
            }

        )
    else:
        ctx.run(
            f'gunicorn --config {package_name}/conf/gunicorn.py --pythonpath "$(pwd)"'
            f' {package_name}.app:app',
            echo=True,
        )


@task
def flake8(ctx):
    """Run flake8 with proper exclusions."""
    ctx.run(
        f'flake8 {package_name}/',
        echo=True,
    )


@task
def pydocstyle(ctx):
    """Run pydocstyle with proper exclusions."""
    cmd = f'find {package_name}/'
    ctx.run(
        cmd + ' -type f \\( -path "*/migrations/*" -o -path "*/local_settings.py" '
              '-o -path "*/tests/*" -o -path "*/utils/functional.py" \\) '
              '-prune -o -name "*.py" -exec pydocstyle --explain "{}" \\+',
        echo=True,
    )


@task
def bandit(ctx):
    """Run bandit with proper exclusions."""
    ctx.run(
        f'bandit -i -r --exclude local_settings.py --exclude utils/functional.py '
        f'{package_name}/',
        echo=True,
    )


@task
def lint_docker(ctx):
    """Lint Dockerfile."""
    ctx.run('sudo docker run --rm -i hadolint/hadolint < Dockerfile', echo=True,
            pty=True, echo_stdin=False)


@task(flake8, pydocstyle, bandit)
def lint(ctx):
    """Lint code and static analysis."""


@task
def build(ctx, tag='latest', for_tests=False):
    """Build Docker image."""
    cmd = ['sudo', 'docker', 'build', '--compress', '--pull', '--rm', '--tag',
           f'{REGISTRY}:{tag}']
    if for_tests:
        cmd.extend(['--build-arg', 'BUILD_FOR_TESTS=1'])
    ctx.run(f'{" ".join(cmd)} .', echo=True, pty=True, echo_stdin=False)


@task
def clean(ctx):
    """Remove all temporary and compiled files."""
    remove = (
        'build',
        'dist',
        '*.egg-info',
        '.coverage',
        'cover',
        'htmlcov',
    )
    ctx.run(f'rm -vrf {" ".join(remove)}', echo=True)
    ctx.run('find . -type d -name "__pycache__" -exec rm -rf "{}" \\+', echo=True)
    ctx.run('find . -type f -name "*.pyc" -delete', echo=True)


@task
def tests(ctx, coverage=False, slow=False):
    """Run tests."""
    env = {
        f'{ENV_PREFIX}TESTING': 'true',
    }
    cmd = 'nose2 -v'
    if coverage:
        cmd = f'{cmd} --with-coverage --coverage-report html'

    ctx.run(cmd, env=env)

    if coverage:
        ctx.run('coverage report --skip-empty --show-missing')

    if slow:
        ctx.run(
            'schemathesis run --hypothesis-deadline 2000 -c all -E trainers '
            '--hypothesis-max-examples 1000 --validate-schema false '
            '--show-errors-tracebacks http://127.0.0.1:8000/openapi.json',
            echo=True,
            pty=True,
        )
        # There's an issue w/ Pydantic that incorrectly generates the schema for
        # pokemon endpoint so omit schema conformance test.
        # See https://github.com/samuelcolvin/pydantic/issues/1270
        ctx.run(
            'schemathesis run --hypothesis-deadline 2000 -c not_a_server_error '
            '-c status_code_conformance -c content_type_conformance -E pokemon '
            '--hypothesis-max-examples 1000 --validate-schema false '
            '--show-errors-tracebacks http://127.0.0.1:8000/openapi.json',
            echo=True,
            pty=True,
        )


@task(
    aliases=['cc'],
    help={
        'complex': 'filter results to show only potentially complex functions (B+)',
    }
)
def cyclomatic_complexity(ctx, complex_=False):
    """Analise code Cyclomatic Complexity using radon."""
    # Run Cyclomatic Complexity
    cmd = 'radon cc -s -a'
    if complex_:
        cmd += ' -nb'
    ctx.run(f'{cmd} {package_name}', pty=True)


@task
def cockroachdb(ctx):
    """Run a temporal CockroachDB container."""
    ctx.run(
        'sudo docker run --rm --name cockroachdb --tmpfs /tmp --detach '
        'cockroachdb/cockroach:latest start-single-node --listen-addr 0.0.0.0 '
        '--temp-dir /tmp --insecure',
        echo=True,
        pty=True,
        echo_stdin=False,
    )
    ctx.run('sleep 4')
    ctx.run(
        'sudo docker exec cockroachdb ./cockroach sql --insecure -e '
        '"CREATE DATABASE IF NOT EXISTS testdb; CREATE USER testuser; '
        'GRANT SELECT, INSERT, DELETE, UPDATE, CREATE, DROP ON DATABASE testdb '
        'TO testuser;"',
        echo=True,
        pty=True,
        echo_stdin=False,
    )
    print('Container name: cockroachdb')
    print('Container IP:')
    ctx.run(
        'sudo docker inspect -f '
        '"{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" cockroachdb',
        pty=True,
        echo_stdin=False,
    )
    print('Database: testdb')
    print('Username: testuser')
    print('Password: <no password>')
    ctx.run(
        'sudo docker logs -f cockroachdb',
        warn=True,
        echo=True,
        pty=True,
        echo_stdin=False,
    )
    ctx.run(
        'sudo docker container stop cockroachdb',
        echo=True,
        pty=True,
        echo_stdin=False,
    )
