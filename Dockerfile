# Build stage
FROM registry.gitlab.com/nevrona/public/poetry-docker:1.0.5 AS builder

ARG PYTHONUNBUFFERED=1
ARG BUILD_FOR_TESTS=0
ARG POETRY_VIRTUALENVS_CREATE=0

RUN apk update \
    && apk add --no-cache \
        build-base==0.5-r1 \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*

WORKDIR /src/
COPY . .
# hadolint ignore=SC2046
RUN poetry export --format requirements.txt --output app.requirements.txt $([ "$BUILD_FOR_TESTS" = "1" ] && printf "%s" "--dev") \
    # build app
    && poetry build --format wheel

# Install app
FROM python:3.8.2-alpine3.11 as installer

ARG PYTHONUNBUFFERED=1

RUN apk update \
    && apk add --no-cache \
        build-base==0.5-r1 \
        # argon2/blake2
        libffi-dev==3.2.1-r6 \
        # <>
        # pyscopg2
        postgresql-dev==12.2-r0 \
        # <>
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*

WORKDIR /tmp/app/
# Bring files from the build stage
COPY --from=builder /src/app.requirements.txt ./
COPY --from=builder "/src/dist/*-py3-none-any.whl" ./

# Install app
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3013
RUN pip install --no-cache-dir --requirement app.requirements.txt \
    && pip install --no-cache-dir "$(find . -type f -name "*-py3-none-any.whl")"

# Final stage
FROM registry.gitlab.com/nevrona/public/gunicorn-docker:20.0.4

LABEL org.opencontainers.image.title="PokeAPI"
LABEL org.opencontainers.image.description="A sample API based on Pokemon theme to test frontend candidates"
LABEL org.opencontainers.image.vendor="Nevrona S. A."
LABEL org.opencontainers.image.authors="HacKan <hackan@nevrona.org>"
LABEL org.opencontainers.image.version="0.1.0"
LABEL org.opencontainers.image.ref.name="0.1.0"
LABEL org.opencontainers.image.licenses="MPL-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/nevrona/public/pokeapi"
LABEL org.opencontainers.image.url="https://registry.gitlab.com/nevrona/public/pokeapi"
LABEL org.opencontainers.image.created=""
LABEL org.opencontainers.image.revision=""
LABEL org.nevrona.url="https://nevrona.org"

ENV PYTHONUNBUFFERED 1
# Disable breakpoints (shouldn't be any but just in case)
ENV PYTHONBREAKPOINT 0
# Disable creating new pyc files
ENV PYTHONDONTWRITEBYTECODE 1

# Define limited user
ARG APP_USER=app
ARG APP_ROOT=/srv/app
ARG APP_USER_UID=2000
# Nginx Group
ARG APP_USER_GID=101

RUN apk update \
    && apk add --no-cache \
        # pyscopg2
        libpq==12.2-r0 \
        # <>
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*

RUN addgroup -g "${APP_USER_GID}" "${APP_USER}" \
    && adduser \
        -D \
        -h "${APP_ROOT}" \
        -s /sbin/nologin \
        -u "${APP_USER_UID}" \
        -G "${APP_USER}" "${APP_USER}"

# Bring site-packages from previous stage
COPY --from=installer /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

# Create required dirs so permissions are correct
RUN mkdir "${APP_ROOT}/static" \
    && chown -R "${APP_USER_UID}:${APP_USER_GID}" "${APP_ROOT}/"

# Copy the entrypoint
COPY --chown=root:root docker-entrypoint.ash /usr/local/bin/

# Change to the project root
WORKDIR /usr/local/lib/python3.8/site-packages/

EXPOSE 8000
# Drop privs
USER ${APP_USER}
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.ash"]
CMD ["gunicorn", "--config", "pokeapi/conf/gunicorn.py", "--pythonpath", "$(pwd)", "pokeapi.app:application"]

