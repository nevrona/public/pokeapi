# PokeAPI

A basic CRUD API themed with Pokemon to test a front-end candidate.  
The main idea is to connect this API and combine it with [pokeapi.co](https://pokeapi.co/) to retrieve Pokémon details.

## Sample exercise

You wake up like every day, but discover that someone has sent you a postal package.

When you open it you discovered something that seems to be a PokéDex, along with an instruction (API) on how to put it all together:

*WRITE API URL HERE*

Apparently, the instructions have a secret mission that requires your knowledge to design special software.

You must understand how to extend such software so that it can (in no particular order):

- [ ] Create Trainers.
- [ ] Get Trainers and display its information.
- [ ] Show all Trainers.
- [ ] Show all Pokémon with and without Trainer (allow differentiating between both situations).
- [ ] Assign a Pokémon to Trainers.
- [ ] Transfer Pokémon between Trainers.
- [ ] Remove Pokémon from Trainers.
- [ ] Delete Trainers.

It is required that you create an application in JS using your preferred framework.  
It should be possible to use a browser to access the information.

Considerations:

* Pokémon information must come from `https://pokeapi.co/api/v2/pokemon/:number/`.
* Show only the following fields for any Pokémon:
  * *name*
  * *type/s* (comma-separated)
  * *number*
  * *picture*
* Always show all the fields above for any Pokémon whenever a Pokémon is shown.
* Only Pokémon from Generation I are accepted.
* Run your local server at `127.0.0.1:3000` (why? what happens if another port or `localhost` is used?).

Extra points for tests, docstrings, type hints, structure, completeness, commit message, using a package and deps manager.

## Developing

* Clone the repo
* Create a virtual environment
* Install dependencies with `poetry install`
* Run dev server with `inv runserver --development`

You can use env vars to set settings or check the local_settings.sample.py file and rename it to local_settings.py in `conf` module. The project runs as-is, all required settings are set to some useful default.

By default an SQLite database is used but you may want to try using CockroachDB. To do so you can run it in a container with `inv cockroachdb` and then specify in your local settings:

```
DATABASE_NAME: str = 'testdb'
DATABASE_USER: str = 'testuser'
DATABASE_HOST: str = '<container ip>'
```

If you need to run this project locally to, say, work on a front-end application you can instead use the Docker image either by building (`inv build`) or checking [our images repo](https://gitlab.com/nevrona/public/pokeapi/container_registry).

### Code quality

Run `inv lint` to statically check the code and `inv tests -c` to run tests with coverage (optionally use `inv tests -s` to run all tests including **slow** ones).

### Committing

Before committing changes make sure everything works as expected by:

* Creating necessary tests
* Running `inv lint` to lint the code
* Running `inv tests` to test the code

If the linter complains about *code too complex*, run `inv cc -c` (or the long expression `inv cyclomatic-complexity --complex`) for more information.

## Deploy

Check the corresponding [readme](deploy/README.md).

## License

*PokeAPI* is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE). Derived works may link back to the canonical repository: `https://gitlab.com/nevrona/public/pokeapi`.

**Neither the name of Nevrona S. A. nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.**
