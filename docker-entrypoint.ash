#!/bin/ash
set -eu

bailout() {
    echo "Error: $*"
    exit 1
}

# Collect statics
cp -fuR /usr/local/lib/python3.8/site-packages/pokeapi/static "${HOME}/"
chown -R "$(id -nu):$(id -ng)" "${HOME}/static"

echo "Executing: $*"
exec "$@"
