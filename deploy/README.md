# Deploy

## Requirements

- ansible = 2.9

## Server Requirements

- docker
- docker-compose

## Deployment

Two methods of deployment are support: `by ansible-playbook` command or `by gitlab-ci`

### Ansible Deployment

You must add this Variables on `hosts` file or in `vars/main.yml` on the role directory if your plan is run only ansible.

- fqdn:                           "domain.com www.domain.com"
- installation_dir:               "/srv/pvo-auth-bkd"
- registry_domain:                "registry.gitlab.com"
- registry_path:                  "..."
- registry_user:                  "token_user"
- registry_pass:                  "token_pass"
- container_tag:                  "develop"
- proxy_rule_name:                "nva_pokeapi_bkd"
- pka_development_mode:           false
- pka_debug:                      false
- pka_loglevel:                   "DEBUG"
- pka_database_name:              "pvo-auth-bkd"
- pka_database_user:              "pvo-auth-bkd"
- pka_database_password:          "some_password"
- pka_database_host:              "cockroachdb"
- pka_database_port:              "26257"
- pka_database_params:            "sslmode=disable"
- pka_allowed_hosts:              "another.domain.com another.site.com"
- pka_api_prefix:                 "/auth"
- pka_openapi_url_path:           "/openapi.json"
- pka_docs_url_path:              "/docs"
- pka_redoc_url_path:             "/redoc"
- pka_static_url_path:            "/static"
- pka_app_identifier:             "Somethig_to_identinfy"
- pka_static_url:                 ""

Only for development purposes

- pka_enable_https_redirect:      "false"
- pka_allow_frontend_local:       "false"

Then run `ansible-playbook deploy.yml -i hosts`

Enjoy!

###  Gitlab-CI Deployment 

If your plan is run the deployment from a gitlab-ci you need to ensure that this variables are available for the runtime. Add this varaibles following [this manual](https://gitlab.com/help/ci/variables/README#variables).

- __HOSTO__: `FQDN` or `IP`
- __SSH_KEY__: `private ssh key whit an extra last blank new line`
- __SSH_USER__: `user name with ssh access to the host`

Also every ansible variable has an UPPERCASE par that you must need to complete.

Some examples are:
- __FQDN__
- __INSTALLATION_DIR__
- __REGISTRY_DOMAIN__

> __Note:__ One special variable was added for development purposes. 
> _Be careful and don't __shoot__ your own feet_.
> - __PKA_ENABLE_HTTPS_REDIRECT__
