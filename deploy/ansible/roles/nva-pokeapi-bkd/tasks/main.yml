---
# tasks file for nva-pokeapi-bkd
- name:  Create a new docker network for Traefik (IPv4 only)
  docker_network:
    name: proxy_internal_network
    internal:

- name: "check if directory {{ installation_dir }} exists"
  stat:
    path: "{{ installation_dir }}"
  register: main_directory

- name: Create required folders
  file:
    path: "{{ installation_dir }}"
    state: directory
    recurse: yes
    mode: '0700'
  when: main_directory.stat.exists == False

- name: Create compose file
  template:
    src: docker-compose.yml.j2
    dest: "{{ installation_dir }}/docker-compose.yml"
    mode: '0600'

- name: Create app env file
  template:
    src: app.env.j2
    dest: "{{ installation_dir }}/.app.env"
    mode: '0600'

- name: Create Nginx vhost file
  template:
    src: nginx-vhost.conf.j2
    dest: "{{ installation_dir }}/vhost.conf"
    mode: '0644'

- name: Log into Gitlab registry and force re-authorization
  docker_login:
    registry: "{{ registry_domain }}"
    username: "{{ registry_user }}"
    password: "{{ registry_pass }}"

- name: Get Image
  docker_image:
    name: "{{ registry_domain }}{{ registry_path }}"
    tag: "{{ container_tag }}"
    source: pull

- name: Pull Auxiliar Images
  command: /usr/bin/env docker-compose pull
  args:
    chdir: "{{ installation_dir }}"

- name: Start DB Container
  command: /usr/bin/env docker-compose up -d cockroachdb
  args:
    chdir: "{{ installation_dir }}"
  # Only 2 states are possible: failed or up-to-date
  changed_when: False

- name: Is DB Initialized?
  command: /usr/bin/env docker-compose exec cockroachdb sh -c "./cockroach sql --insecure -e \"SELECT true FROM pg_database where datname='{{ pka_database_name }}';\" | /usr/bin/env grep true || /usr/bin/env echo false"
  args:
    chdir: "{{ installation_dir }}"
  register: is_db_initialized
  changed_when: is_db_initialized.stdout == "false"

- name: Initializing DB
  command: /usr/bin/env docker-compose exec cockroachdb ./cockroach sql --insecure -e "CREATE DATABASE IF NOT EXISTS {{ pka_database_name }}; CREATE USER {{ pka_database_user }}; GRANT SELECT, INSERT, DELETE, UPDATE, CREATE, DROP ON DATABASE {{ pka_database_name }} TO {{ pka_database_user }};"
  args:
    chdir: "{{ installation_dir }}"
  register: db_initialization
  when: is_db_initialized is changed

- debug:
    msg: "{{ db_initialization }}"
  when: is_db_initialized is changed

- name: Start Containers
  command: /usr/bin/env docker-compose up -d
  args:
    chdir: "{{ installation_dir }}"
  # Only 2 states are possible: failed or up-to-date
  changed_when: False
  register: output

- debug:
    msg: "{{ output }}"

- name: Logout from Gitlab registry
  docker_login:
    registry: "{{ registry_domain }}"
    state: absent
