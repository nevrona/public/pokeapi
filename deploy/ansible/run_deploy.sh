#!/usr/bin/env bash

# VARIABLES
vars=()

# FUNCTIONS
auto_var_builder() {
    while read -r item; do
        item_name="${item%%=*}"
        { [ "${item_name}" == "${1}" ] || [ -z "${!item_name}" ]; } \
        || vars+=("${item_name,,}=\'\${${item_name}}\'")
    done <<< "$(env | grep "$1")"
}

# RUN RUN 4 FUN
[ -z "${1}" ] && printf "PREFIX is not defined\n" && exit 1

{ [ -z "${HOSTO}" ] && printf "HOSTO is not defined\n" && exit 1; } || vars+=("target=\${HOSTO}")

{ [ -z "${SSH_USER}" ] && printf "SSH_USER is not defined\n" && exit 1; } || vars+=("ansible_ssh_user=\${SSH_USER}")

{ [ -z "${SSH_KEY}" ] && printf "SSH_KEY is not defined\n" && exit 1; } || vars+=("ansible_ssh_private_key_file=\${SSH_KEY}")

{ [ -z "${FQDN}" ] && printf "FQDN is not defined\n" && exit 1; } || vars+=("fqdn=\'\${FQDN}\'")

{ [ -z "${INSTALLATION_DIR}" ] && printf "INSTALLATION_DIR is not defined\n" && exit 1; } || vars+=("installation_dir=\${INSTALLATION_DIR}")

[ -z "${REGISTRY_DOMAIN}" ] && printf "REGISTRY_DOMAIN is not defined\n" && exit 1

[ -z "${REGISTRY_PATH}" ] && printf "REGISTRY_PATH is not defined\n" && exit 1

auto_var_builder "REGISTRY"

{ [ -z "${CONTAINER_TAG}" ] && printf "CONTAINER_TAG is not defined\n" && exit 1; } || vars+=("container_tag=\${CONTAINER_TAG}")

{ [ -z "${CONTAINER_TAG}" ] && printf "CONTAINER_TAG is not defined\n" && exit 1; } || vars+=("proxy_rule_name=\${PROXY_RULE_NAME}")

auto_var_builder "${1}"

eval "ansible-playbook deploy.yml -i hosts --extra-vars \"ansible_ssh_common_args='-o StrictHostKeyChecking=no' ansible_ssh_transfer_method=scp ansible_python_interpreter=/usr/bin/python3 ${vars[*]}\"" || exit 1

exit 0