# PokeAPI Backend Auto Deploy

## Requirements

- ansible = 2.9

## Server Requierements

- docker
- docker-compose

## Deployment

2 methods of deployment are support: `by ansible-playbook` command or `by gitlab-ci`

### Ansible Deployment

You must add this Variables on `hosts` file or in `vars/main.yml` on the role directory if your plan is run only ansible.

- __fqdn__: `"domain.com www.domain.com"`
- __installation_dir__: `"/srv/nva-pokeapi-bkd"`
- __registry_domain__: `"registry.gitlab.com"`
- __registry_path__: `"/nevrona/..."`
- __registry_user__: `"token_user"`
- __registry_pass__: `"token_pass"`
- __container_tag__: `"develop"`
- __proxy_rule_name__: `"nva_pokeapi_bkd"`
- __pka_development_mode__: `false`
- __pka_shared_secret_key__: `"ASDFFAASDsdaf14e23dDc22332"`
- __pka_debug__: `false`
- __pka_loglevel__: `"DEBUG"`
- __pka_database_name__: `"..."`
- __pka_database_user__: `"..."`
- __pka_database_password__: `"some_password"`
- __pka_database_host__: `"cockroachdb"`
- __pka_database_port__: `"26257"`
- __pka_database_params__: `"sslmode=disable"`
- __pka_allowed_hosts__: `"another.domain.com another.site.com"`
- __pka_hsts_seconds__: `"86400"`
- __pka_api_prefix__: `"/api"`
- __pka_openapi_url_path__: `"/openapi.json"`
- __pka_docs_url_path__: `"/docs"`
- __pka_redoc_url_path__: `"/redoc"`
- __pka_static_url_path__: `"/static"`
- __pka_app_identifier__: `"Somethig_to_identinfy"`

Then run `ansible-playbook deploy.yml -i hosts`

Enjoy!

###  Gitlab-CI Deployment 
If your plan is run the deployment from a gitlab-ci you need to ensure that this variables are available for the runtime. Add this variables following [this manual](https://gitlab.com/help/ci/variables/README#variables).

- __HOSTO__: `FQDN` or `IP`
- __SSH_KEY__: `private ssh key whit an extra last blank new line`
- __SSH_USER__: `user name with ssh access to the host`

Also every ansible variable has an UPPERCASE par that you must need to complete.

Some examples are:
- __FQDN__
- __INSTALLATION_DIR__
- __REGISTRY_DOMAIN__

> __Note:__ One special variable was added for development purposes. 
> _Be careful and don't __shoot__ your own feet_.
> - __PKA_ENABLE_HTTPS_REDIRECT__
